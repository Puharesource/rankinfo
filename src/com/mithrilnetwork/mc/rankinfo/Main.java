package com.mithrilnetwork.mc.rankinfo;

import com.mithrilnetwork.mc.rankinfo.commands.CommandRank;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
    private Permission perms;

    public void onEnable()
    {
        if(!setupPermissions())
        {
            getLogger().severe("Vault not present! Disabling plugin!");
            this.getPluginLoader().disablePlugin(this);
        }

        getCommand("rank").setExecutor(new CommandRank(this));
    }

    public void onDisable()
    {

    }

    public Permission getPermsissions()
    {
        return perms;
    }

    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null)
            perms = permissionProvider.getProvider();
        return (perms != null);
    }
}
