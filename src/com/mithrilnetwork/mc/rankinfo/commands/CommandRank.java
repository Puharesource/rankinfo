package com.mithrilnetwork.mc.rankinfo.commands;

import com.mithrilnetwork.mc.rankinfo.Main;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandRank implements CommandExecutor
{
    private Main plugin;

    public CommandRank(Main plugin)
    {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(cmd.getName().equalsIgnoreCase("rank"))
        {
            if(args.length != 1)
            {
                sender.sendMessage(ChatColor.RED + "Wrong usage! Correct usage:");
                sender.sendMessage(ChatColor.RED + "    /rank <name>");
                return true;
            }

            Player player = getPlayerByName(args[0]);

            if(player != null)
                sender.sendMessage(ChatColor.GREEN + player.getDisplayName() + ChatColor.GREEN + " is a " + plugin.getPermsissions().getPrimaryGroup(player));
            else
                sender.sendMessage(ChatColor.RED + args[0] + " is not online!");

            return true;
        }
        return false;
    }

    private Player getPlayerByName(String name)
    {
        Player[] players = Bukkit.getServer().getOnlinePlayers();
        Player player = null;
        for(int i = 0; players.length > i; i++)
        {
            if(StringUtils.containsIgnoreCase(players[i].getName(), name))
            {
                player = players[i];
                break;
            }
        }
        return player;
    }
}
